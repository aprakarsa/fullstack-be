import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import UserRoute from "./routes/UserRoute.js";

import "dotenv/config";

const atlasUri = process.env.ATLAS_URI;

const app = express();
mongoose.connect(atlasUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (error) => console.log(error));
db.once("open", () => console.log("Connected to Database"));

app.use(cors());
app.use(express.json());
app.use(UserRoute);

app.listen(5000, () => {
    console.log("Server has started on port 5000..");
});
